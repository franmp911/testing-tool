package com.fjmpaez.testingtool;

import static com.fjmpaez.testingtool.util.FileUtils.readFileFromClasspath;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import io.restassured.http.ContentType;

@RunWith(MockitoJUnitRunner.class)
public class TestingToolApplicationTests {

    private static final String USER = "adidas";
    private static final String PASSWORD = "4d1d4s";

    private static final String PRODUCT_ID = "C77154";

    private static final String GET_PRODUCT_URL = "http://localhost:8667/v1.0/product/{product_id}";

    private static final String GET_PRODUCTS_REVIEWS_URL = "http://localhost:8666/v1.0/review";
    private static final String GET_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review/{product_id}";
    private static final String POST_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review";
    private static final String PUT_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review/{product_id}";
    private static final String DELETE_PRODUCT_REVIEW_URL = "http://localhost:8666/v1.0/review/{product_id}";

    @Test
    public void getProductNotValidForAdidas() {

        given().get(GET_PRODUCT_URL, "C77154aaaaaa")

                .then().statusCode(404).body("error_code", equalTo("002003"))
                .body("description", equalTo("product not found"));
    }

    @Test
    public void getProductWithReview() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_10_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL);

        given().get(GET_PRODUCT_URL, PRODUCT_ID)

                .then().statusCode(200).body("id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(10)).body("reviews_count", equalTo(2));
    }

    @Test
    public void getProductWithoutReview() {

        given().auth().basic(USER, PASSWORD).delete(DELETE_PRODUCT_REVIEW_URL, PRODUCT_ID);

        given().get(GET_PRODUCT_URL, PRODUCT_ID)

                .then().statusCode(200).body("id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(null)).body("reviews_count", equalTo(null));
    }

    @Test
    public void createAndGetReview() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_10_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL).then().statusCode(201)
                .body("product_id", equalTo(PRODUCT_ID)).body("reviews_avg_score", equalTo(10))
                .body("reviews_count", equalTo(2));

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(200).body("product_id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(10)).body("reviews_count", equalTo(2));
    }

    @Test
    public void createReviewWrongAvgScore() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_11_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL).then().statusCode(400)
                .body("description", equalTo("invalid parameter: avgScore"));
    }

    @Test
    public void getReviewUpdated() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_10_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL);

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(200).body("product_id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(10)).body("reviews_count", equalTo(2));

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_9_20.json"))
                .contentType(ContentType.JSON).put(PUT_PRODUCT_REVIEW_URL, PRODUCT_ID);

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(200).body("product_id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(9)).body("reviews_count", equalTo(20));
    }

    @Test
    public void updateReviewWrongAvgScore() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_10_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL);

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(200).body("product_id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(10)).body("reviews_count", equalTo(2));

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_11_2.json"))
                .contentType(ContentType.JSON).put(PUT_PRODUCT_REVIEW_URL, PRODUCT_ID).then()
                .statusCode(400).body("description", equalTo("invalid parameter: avgScore"));

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(200).body("product_id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(10)).body("reviews_count", equalTo(2));
    }

    @Test
    public void updateReviewWrongWithDifferentProductIdInPath() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_10_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL);

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(200).body("product_id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(10)).body("reviews_count", equalTo(2));

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_9_20.json"))
                .contentType(ContentType.JSON).put(PUT_PRODUCT_REVIEW_URL, "XXXX").then()
                .statusCode(400).body("error_code", equalTo("001002"))
                .body("description", equalTo("invalid parameter: product_id"));

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(200).body("product_id", equalTo(PRODUCT_ID))
                .body("reviews_avg_score", equalTo(10)).body("reviews_count", equalTo(2));
    }

    @Test
    public void getReviewNonExists() {

        given().auth().basic(USER, PASSWORD).delete(DELETE_PRODUCT_REVIEW_URL, PRODUCT_ID);

        given().get(GET_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(404).body("error_code", equalTo("001003"))
                .body("description", equalTo("review not found"));
    }

    @Test
    public void deleteReview() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_10_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL);

        given().auth().basic(USER, PASSWORD).delete(DELETE_PRODUCT_REVIEW_URL, PRODUCT_ID).then()
                .statusCode(204);
    }

    @Test
    public void deleteReviewNonExists() {

        given().auth().basic(USER, PASSWORD).delete(DELETE_PRODUCT_REVIEW_URL, PRODUCT_ID);

        given().auth().basic(USER, PASSWORD).delete(DELETE_PRODUCT_REVIEW_URL, PRODUCT_ID)

                .then().statusCode(404).body("error_code", equalTo("001003"))
                .body("description", equalTo("review not found"));
    }

    @Test
    public void getReviews() {

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_C77154_10_2.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL).then().statusCode(201)
                .body("product_id", equalTo(PRODUCT_ID)).body("reviews_avg_score", equalTo(10))
                .body("reviews_count", equalTo(2));

        given().auth().basic(USER, PASSWORD)
                .body(readFileFromClasspath("data/request/POST_M20324_6_11.json"))
                .contentType(ContentType.JSON).post(POST_PRODUCT_REVIEW_URL).then().statusCode(201)
                .body("product_id", equalTo("M20324")).body("reviews_avg_score", equalTo(6))
                .body("reviews_count", equalTo(11));

        given().get(GET_PRODUCTS_REVIEWS_URL).then().statusCode(200).body("reviews.product_id",
                hasItems("C77154", "M20324"));

    }

}
